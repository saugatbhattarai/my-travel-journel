import logo from './logo.svg';
import './App.css';
import MainComponent from "./MainComponent";
import NavBar from './NavBar';
import data from "./data";

function App() {
  const travelDairy = data.map(travel => 
      <MainComponent 
        key={travel.id}
        travel={travel}
      />
    )
  return (
    <div className='container'>
      <NavBar />
      <section className='main--content'>
      {travelDairy}
      </section>
      
    </div>
  );
}

export default App;
